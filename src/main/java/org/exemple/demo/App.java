package org.exemple.demo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException
    {
        System.out.println( "Hello World!" );
        Properties vPro=new Properties();
        InputStream vInputStream=null;
        try {
            vInputStream=App.class.getResourceAsStream("/info.properties");
            vPro.load(vInputStream);
        }finally {
            if(vInputStream !=null){
                vInputStream.close();
            }
        }
        System.out.println("Application version : " + vPro.getProperty("org.exemple.demo.version" , "?"));
    }
}
